<?php
ob_start();
?> 

<div class="card mb-3">
  <div class="card-body">
	<h6 class="card-title">Country environment and development profiles</h6>
	<div class="card-text">
	  Useful external links to Country Environment and Development Profiles.
	  <br>
	  <hr>
	  <div id="wrapper-links" style="display: block;"> 
		<div class="links-group">
		  <h5>Country statistics</h5>
			<ul class="profile-links">
				<li><a class="link-un-data" href="http://data.un.org/en/iso/NA.html" target="_blank"> UN Data</a> </li>
				<li><a href="https://databank.worldbank.org/views/reports/reportwidget.aspx?Report_Name=CountryProfile&Id=b450fd57&tbar=y&dd=y&inf=n&zm=n" target="_blank"> World Bank  </a> </li>
				<li><a href="http://www.fao.org/faostat/en/#country" target="_blank"> FAO country statistics </a> </li>
			</ul>
		</div>
		<div class="links-group">
		  <h5>Development indicators</h5>
			<ul class="profile-links">
				<li><a class="link-iso3" href="http://hdr.undp.org/en/countries/profiles/" target="_blank"> UNDP Country Profile</a> </li>
			</ul>
		</div>
		<div class="links-group">
		  <h5>Environment and biodiversity indicators</h5>
			<ul class="profile-links">
				<li><a class="link-iso2" href="https://www.cbd.int/countries/?country=NA" target="_blank"> UN Convention on Biological Diversity (CBD) country profile</a> </li>
				<li><a class="link-country-name" href="http://datazone.birdlife.org/country/Namibia" target="_blank"> Country profile by BirdLife International </a> </li>
				<li><a class="link-iso2" href="http://www.gbif.org/country/NA" target="_blank"> Global Biodiversity Information Facility (GBIF) country report</a> </li>
			</ul>
		</div>
		<div class="links-group">
		  <h5>Protected areas</h5>
			<ul class="profile-links">
				<li><a class="link-iso2" href="https://www.cbd.int/protected/implementation/actionplans/country/?country=NA" target="_blank"> CBD Programme of Work on Protected Areas (PoWPA) Action Plan </a> </li>
				<li><a class="link-iso2" href="https://www.protectedplanet.net/country/NA" target="_blank"> UNEP-WCMC Country Profile from the World Database on Protected Areas (WDPA) </a> </li>
				<li><a class="link-country-name" href="https://www.ecolex.org/result/?q=&amp;type=legislation&amp;xkeywords=protected+area&amp;xcountry=Namibia" target="_blank"> ECOLEX: Country environmental legislation </a> </li>
			</ul>
		</div>
	  </div>
	</div>
  </div>
</div> 

<?php $ProfileCustom = ob_get_clean();?>