<?php
namespace Drupal\biopama_global_data\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Provides route responses for the BIOPAMA DOPA country page.
 */
class BiopamaGlobalDataController extends ControllerBase {
  public function country($iso_code = null) {
    $element = array(
	  '#theme' => 'page_dopa_country',
      '#iso_code' => $iso_code,
    );
    return $element;
  }
  public function pa($wdpa_id = null) {
    $element = array(
	  '#theme' => 'page_dopa_pa',
    );
    return $element;
  }
  public function ecoregion($ecoregion_id = null) {
    $element = array(
	  '#theme' => 'page_dopa_ecoregion',
    );
    return $element;
  }
  public function reportCountry() {
    $element = array(
	  '#theme' => 'page_reporting',
    );
    return $element;
  }
}
