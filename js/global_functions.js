var chartTooltip = {};

var chartToolbox = {
	show: true,
	right: 20,
	feature: {
		saveAsImage: {
			title: 'Image',
		}
	}
};
(function($){
	$.fn.createDataTable = function(cardTwigName, tableData){
	
		if ($.fn.DataTable.isDataTable( '#dopa-data-table-'+cardTwigName ) ) { //to see if a datatable is here. Kill it if it is found
			if (cardTwigName == "TerrestrialEcoregions"){  
				var table = $('#dopa-data-table-MarineEcoregions').DataTable();
				table.destroy();
				$('#dopa-data-table-'+cardTwigName).empty();
			}
			var table = $('#dopa-data-table-'+cardTwigName).DataTable();
			table.destroy();
			$('#dopa-data-table-'+cardTwigName).empty();
		}
		if (typeof activeIndicators !== 'undefined'  ){
			activeIndicators[cardTwigName+'Table'] = tableData;
			activeIndicators[cardTwigName+'Table'].type = 'table';
		}
		var dataTableID = '#dopa-data-table-'+cardTwigName;
		dt_table = $(dataTableID).DataTable({
			data: tableData.data,
			columns : tableData.columns,
			columnDefs: tableData.columnDefs,
			order: tableData.defaultSort,
			dom: 'Bfrtip',
			paging: tableData.isComplex,
			searching: tableData.isComplex,
			ordering:  tableData.isComplex,
			info: tableData.isComplex,
			buttons: [ 
				{
					extend: 'print',
					title: tableData.title,
					messageBottom: tableData.attribution,
				},{
					extend: 'excel',
					title: tableData.title,
					messageBottom: tableData.attribution,
				},{
					extend: 'copy',
					title: tableData.title, 
					messageBottom: tableData.attribution,
				},{
					extend: 'csv',
					title: tableData.title,
					messageBottom: tableData.attribution,
				},
			],
		});
		$('button.dt-button').removeClass("dt-button").addClass('btn btn-info');
		$('div.dataTables_filter').find('input[type="search"]').addClass('form-control table-search');
		$(dataTableID).find("#mini-loader-wrapper").remove();
	}
	$.fn.createReportDataTable = function(cardTwigName, tableData){
		if ($.fn.DataTable.isDataTable( '#dopa-data-table-'+cardTwigName ) ) { //to see if a datatable is here. Kill it if it is found
			if (cardTwigName == "TerrestrialEcoregions"){  
				var table = $('#dopa-data-table-MarineEcoregions').DataTable();
				table.destroy();
				$('#dopa-data-table-'+cardTwigName).empty();
			}
			var table = $('#dopa-data-table-'+cardTwigName).DataTable();
			table.destroy();
			$('#dopa-data-table-'+cardTwigName).empty();
		}
		var dataTableID = '#dopa-data-table-'+cardTwigName;
		dt_table = $(dataTableID).DataTable({
			data: tableData.data,
			columns : tableData.columns,
			columnDefs: tableData.columnDefs,
			order: tableData.defaultSort,
			paging: false,
			searching: false,
			ordering:  false,
			info: false,
		});
	}
	$.fn.updateCellColors = function(cardTwigName, colors){
		colors.forEach(function (item, index) {
			$('.' + cardTwigName + "-cell-color-" + index).css("color", item);
		});
	}
		
	$.fn.createXYAxisChart = function(cardTwigName, chartData, report = false){ 
		var thisTitle = {
				text: chartData.title,
				show: true,
			}
		var thisTooltip = chartTooltip;
		var thisToolbox = chartToolbox;
		if (report){
			thisTitle = {};
			thisTooltip = {};
			thisToolbox = {};
		}
		if (typeof activeIndicators !== 'undefined'  ){
			activeIndicators[cardTwigName+'Chart'] = chartData;
			activeIndicators[cardTwigName+'Chart'].type = 'chart';
			activeIndicators[cardTwigName+'Chart'].chartType = 'XYAxis';
		}
		var chartDestinationID = 'dopa-data-chart-'+cardTwigName;
		var indicatorChart = echarts.init(document.getElementById(chartDestinationID));
		var option = { 
			title: thisTitle, 
			legend: chartData.legend,
			color: chartData.colors,
			xAxis: [{
				type: chartData.xAxis.type,
				data: chartData.xAxis.data,
				name: chartData.xAxis.title,
				nameLocation: "middle",
				nameGap: 30
			}],
			tooltip: thisTooltip,
			toolbox: thisToolbox,
			yAxis: {
			  name: chartData.yAxisTitle, 
			  nameRotate: 90,
			  nameLocation: "middle",
			  nameGap: 60
			},
			series: chartData.series,
			animationDuration: 600,
		};
		indicatorChart.setOption(option);
	}
	$.fn.createNoAxisChart = function(cardTwigName, chartData, report = false){
		var thisTitle = {
				text: chartData.title,
				show: true,
			}
		var thisTooltip = chartTooltip;
		var thisToolbox = chartToolbox;
		if (report){
			thisTitle = {};
			thisTooltip = {};
			thisToolbox = {};
		}
		if (typeof activeIndicators !== 'undefined'  ){
			activeIndicators[cardTwigName+'Chart'] = chartData;
			activeIndicators[cardTwigName+'Chart'].type = 'chart';
			activeIndicators[cardTwigName+'Chart'].chartType = 'NoAxis';
		}
		//pie charts are VERY similar to line and bar charts, but all the config data is in the series, so we need a separate function 
		var chartDestinationID = 'dopa-data-chart-'+cardTwigName;
		var indicatorChart = echarts.init(document.getElementById(chartDestinationID));
		var option = { 
			title: thisTitle, 
			color: chartData.colors,
			tooltip: thisTooltip,
			toolbox: thisToolbox,
			series: chartData.series,
			animationDuration: 600,
		};
		indicatorChart.setOption(option);
	}
	$.fn.addMapLayer = function(cardID, layerArgs = '', layerID = null){
		if (activeLayers.hasOwnProperty(cardID)){
			return; //just checking if the layer is already in the map. If it is we skip the function.
		}
		if ($(this).hasClass("custom-layer-args")) return; //some layer buttons need to be managed elsewhere. 
		
		var cardTheme = $("#collapse"+cardID).closest( ".wrapper-card" ).prev().text().trim();
		var cardLayers = drupalSettings.dataCardsCountry[cardTheme].data[cardID].layers;
		var cardLayerData;
		
		//if the card has buttons, find the selected/active button and add the layer it is referencing
		//otherwise just add the first layer
		$("#collapse"+cardID).find('button.layer-button.active').attr("data-layer-id");

		if (layerID){
			cardLayerData = cardLayers.find(obj => { return obj.id === layerID }); //assign the layer that got passed in via id
		} else {
			//if the card has buttons, find the selected/active button and add the layer it is referencing
			//otherwise just add the first layer
			if ($("#collapse"+cardID).find('button.layer-button.active').exists()){
				layerID = $("#collapse"+cardID).find('button.layer-button.active').attr("data-layer-id");
				cardLayerData = cardLayers.find(obj => { return obj.id === layerID }); //assign the layer that got passed in via id
			} else { 
				layerID = cardLayers[0].id;
				cardLayerData = cardLayers[0] //if no ID was passed, make it the first one.
			}
		}
		$('.layer-legends-'+cardID).addClass("d-none");
		$('#layer-legend-'+layerID).removeClass("d-none");

		activeLayers[cardID] = cardLayerData;
		mymap.addLayer({
			'id': cardLayerData.id,
			'type': cardLayerData.type,
			'source': {
				'id': cardLayerData.id+'-source',
				'type': cardLayerData.type,
				'tiles': [cardLayerData.url + layerArgs],
				'tileSize': 256,
				'scheme': cardLayerData.scheme,
			},
			'paint': {}
		}, 'countrySelected');
		if (cardLayerData.type == "raster"){
			var slider = document.getElementById('layerSlider-'+layerID);
			slider.addEventListener('input', function (e) {
			mymap.setPaintProperty(
				cardLayerData.id,
				'raster-opacity',
				parseInt(e.target.value, 10) / 100
				);
			});
		}
	}
	$.fn.removeMapLayer = function(cardID){
		if (activeLayers.hasOwnProperty(cardID)){
			mymap.removeLayer(activeLayers[cardID].id);
			mymap.removeSource(activeLayers[cardID].id);
			delete activeLayers[cardID]; 
		}	
	}
	$.fn.removeButtonMapLayer = function(LayerID){
		if (activeLayers.hasOwnProperty(LayerID)){
			mymap.removeLayer(activeLayers[LayerID].id);
			mymap.removeSource(activeLayers[LayerID].id);
			delete activeLayers[LayerID]; 
		}	
	}
	$.fn.sortObject = function(property, AscOrDec) { 
		var sortOrder = 1;
		if (AscOrDec = "dec") sortOrder = -1;
		return function (a,b) {
			var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
			return result * sortOrder;
		}
	}
	var numberOfIndicatorsToPrint = 0;
	$.fn.addIndicatorToReport = function(indicator, type) { 
		$('div#report-link').show(200);
		numberOfIndicatorsToPrint++;
		reportIndicators[indicator+type] =  activeIndicators[indicator+type];
		$('div#report-number').text(numberOfIndicatorsToPrint);
	}
	$.fn.removeIndicatorFromReport = function(indicator, type) { 
		numberOfIndicatorsToPrint--;
		delete reportIndicators[indicator+type];
		if (numberOfIndicatorsToPrint == 0){
			$('div#report-link').hide(200);
		} else {
			$('div#report-number').text(numberOfIndicatorsToPrint);
		}
	}
	$.fn.goToIndicatorReport = function() { 
		console.log(reportIndicators);
		$.jStorage.set("country-report", reportIndicators );
		var value = $.jStorage.get("country-report");
		window.location.href = "/reporting";
		
	}
	$.fn.exists = function () {
    	return this.length !== 0;
	}
})(jQuery);
