/* 

Taken from the Global map settings in the theme library bootstrap_barrio_biopama:biopama-global-vars - /biopama-config/map_settings.js

var mapPointHostURL;
var mapPolyHostURL;

var mapNonACPCountryLayer;
var mapCountryLayer;
var mapCountryPointLayer;
var mapPaLayer;
var mapPaLabelsLayer;
var mapPaPointLayer;
var mapRegionLayer;
var mapSubRegionLayer;
var mapRegionPointLayer;
var mapSubRegionPointLayer;
var mapEEZLayer;
var mapGAULLayer;

 */ 
 
 //test URL
 //https://dopa-services.jrc.ec.europa.eu/services/d6dopa40/administrative_units/get_country_all_inds?format=json&c_un_m49=516
 
/*   
## Global Page Variables ##
so we can set them in the js that will be called later without passing them.
*/
var DopaBaseUrl = "https://dopa-services.jrc.ec.europa.eu/services/d6dopa40/";
var devDopaBaseUrl = 'http://h05-dev-vm8.jrc.it/services/d6dopadev';
var dopaAttribution = "Data Source: DOPA (https://dopa-explorer.jrc.ec.europa.eu/)";
var DOPAgetCountryExtent = "https://rest-services.jrc.ec.europa.eu/services/d6biopamarest/administrative_units/get_country_extent_by_iso?format=json&a_iso=";
var DOPAgetWdpaExtent = "https://rest-services.jrc.ec.europa.eu/services/d6biopamarest/d6biopama/get_wdpa_extent?format=json&wdpa_id=";
var imagePath =  drupalSettings.dopaImgPath;
var mymap; 

var chartTooltip = {};

var chartToolbox = {
	show: true,
	right: 20,
	feature: {
		saveAsImage: {
			title: 'Image',
		}
	}
};

var miniLoadingSpinner = "<div id='mini-loader-wrapper'><div id='mini-loader'></div></div>";
var activeLayers = {}; //To track what's in the map and maybe do something cool with it.
var activeIndicators = []; //To track the indicators and get them in the report
var reportIndicators = {}; 

/*   
## Page ##
*/
(function ($, Drupal) {
	
 	$('[data-toggle="tooltip"]').tooltip(); 
	
	var currentWindowHeight = getWindowHeight(); //this is needed every time the map is resized (on window resize too)
	function getWindowHeight(){
		var height = $(window).height();// - $('#admin-menu-wrapper').outerHeight(true) + $('#messages').outerHeight(true);
		var adminHeight = 0;
		var menuHeight = $("#menu-top-wrapper").height();
		if ($('#toolbar-item-administration-tray')[0]){
			adminHeight =  79 ; //the admin toolbar is exactly 79px tall
		} 
		height = height - (adminHeight + menuHeight + 10); //the 10 is a mystery
		heightPaddingFix = adminHeight + menuHeight + 10; 
		$('#container-page').css('top', heightPaddingFix);
		return height; 
	}

	mapboxgl.accessToken = 'pk.eyJ1IjoiYmxpc2h0ZW4iLCJhIjoiMEZrNzFqRSJ9.0QBRA2HxTb8YHErUFRMPZg';
	mymap = new mapboxgl.Map({
		container: 'container-mapbox-map',
		style: 'mapbox://styles/jamesdavy/cjw25laqe0y311dqulwkvnfoc', //Andrews default new RIS v2 style based on North Star
		attributionControl: true,
		renderWorldCopies: true,
		center: [15, 30],
        zoom: 1.5,
		minZoom: 1,
		maxZoom: 12,
	}); 
	class mapExpandControl {
		onAdd(map) {
			this._map = map;
			this._container = document.createElement('div');
			this._container.className = 'mapboxgl-ctrl mapbox-expand-map';
			this._container.innerHTML = '<button type="button" class="btn btn-expand-map"><i class="fas fa-chevron-left"></i></button>';
			return this._container;
		}
		onRemove() {
			this._container.parentNode.removeChild(this._container);
			this._map = undefined;
		} 
	}
	class mapLoadingSpinnerControl {
		onAdd(map) {
			this._map = map;
			this._container = document.createElement('div');
			this._container.className = 'mapbox-loading-spinner invisible';
			this._container.innerHTML = miniLoadingSpinner;
			return this._container;
		}
		onRemove() {
			this._container.parentNode.removeChild(this._container);
			this._map = undefined;
		} 
	}
	var mapExpand = new mapExpandControl();
	mymap.addControl(mapExpand, 'top-left');	
	var mapLoadingSpinner = new mapLoadingSpinnerControl();
	mymap.addControl(mapLoadingSpinner, 'top-left');
	mymap.addControl(new mapboxgl.FullscreenControl());
	mymap.addControl(new mapboxgl.NavigationControl());
	
	var originalCardContainerWidth = $("#container-data").width();
	$('.btn-expand-map').bind("click", function(){
		$("#container-data").toggleClass('collapse-column');
		$(this).toggleClass('collapseActivated');
		var mapResizing = setInterval(startResizeingMap, 10); //refresh every 10ms 
		
		function startResizeingMap(){
			resizeMap(currentWindowHeight);
		}
		setTimeout(stopResizeingMap, 1020);//stop after 1020ms !important as in the css the transition animation is 1000ms
		function stopResizeingMap(){
			clearInterval(mapResizing);
		}
	});
	
	$(window).resize(function(){
		var currentWindowHeight = getWindowHeight();
		resizeMap(currentWindowHeight);
	});
	

	function resizeMap(height){
		$('#container-map').css('height', height);
		$('#container-data').css('height', height);
		mymap.resize();
	} 
	
	var mapHeight = getWindowHeight();
	resizeMap(mapHeight);

	mymap.on('load', function () {
		mymap.addSource("BIOPAMA_Poly", {
			"type": 'vector',
			"tiles": [mapPolyHostURL+"/{z}/{x}/{y}.pbf"],
			"minZoom": 0,
			"maxZoom": 23,
		});
		mymap.addSource("BIOPAMA_Point", {
			"type": 'vector',
			"tiles": [mapPointHostURL+"/{z}/{x}/{y}.pbf"],
			"minZoom": 0,
			"maxZoom": 23,
		});
		
		mymap.addLayer({
			"id": "wdpaBase",
			"type": "fill",
			"source": "BIOPAMA_Poly",
			"source-layer": mapPaLayer,
			"minzoom": 1,
            "paint": {
                "fill-color": "hsla(87, 47%, 53%, 0.1)",
            }
		});
		
		mymap.addLayer({
			"id": "wdpaAcpPolyLabels",
			"type": "symbol",
			"source": "BIOPAMA_Point",
			"source-layer": mapPaLabelsLayer,
			"minzoom": 5,
            "layout": {
                "text-field": ["to-string", ["get", "NAME"]],
                "text-size": 12,
                "text-font": [
                    "Arial Unicode MS Regular",
                    "Arial Unicode MS Regular"
                ]
            },
            "paint": {
                "text-halo-width": 2,
                "text-halo-blur": 2,
                "text-halo-color": "hsl(0, 0%, 100%)",
                "text-opacity": 1
            }
		}, '');
		mymap.addLayer({
			"id": "wdpaAcpPointLabels",
			"type": "symbol",
			"source": "BIOPAMA_Point",
			"source-layer": mapPaPointLayer,
			"minzoom": 5,
            "layout": {
                "text-field": "{NAME}",
                "text-size": 12,
                "text-padding": 3,
				"text-offset": [0,-1]
            },
            "paint": {
                "text-color": "hsla(213, 49%, 13%, 0.95)",
                "text-halo-color": "hsla(0, 0%, 100%, .9)",
                "text-halo-width": 2,
                "text-halo-blur": 2
            }
		}, '');
		mymap.addLayer({
			"id": "countryHover",
			"type": "line",
			"source": "BIOPAMA_Poly",
			"source-layer": mapCountryLayer,
			"layout": {
				"visibility": "none",
			},
			"paint": {
				"line-color": countryColor,
				"line-width": 2,
			}
		}, 'state-label-lg');
		mymap.addLayer({
			"id": "countrySelected",
			"type": "line",
			"source": "BIOPAMA_Poly",
			"source-layer": mapCountryLayer,
			"layout": {
				"visibility": "none"
			},
			"paint": {
				"line-color": countryColor,
				"line-width": 3
			}
		}, 'state-label-lg');
		//mymap.setLayoutProperty("world_mask", 'visibility', 'visible');
		mymap.addSource('world_mask', {
		'type': 'geojson',
		'data': {
		'type': 'Feature',
		'geometry': {
		'type': 'Polygon',
			"coordinates": [
			  [
				[-180,-90],[180,-90],[180,90],[-180,90],[-180,-90]
			  ]
			]
		}
		}
		});
		mymap.addLayer({
			'id': 'world_mask',
			'type': 'fill',
			'source': 'world_mask',
			'layout': { 'visibility': 'none'},
			'paint': {
			'fill-color': '#000',
			'fill-opacity': 0.7
			}
		}, '');
		
	});
	mymap.on('sourcedata', function(data) {
	  	var sourceImage;
	  	if (data.isSourceLoaded == false){
			$(".mapbox-loading-spinner").removeClass("invisible");
			$(".mapbox-loading-spinner").addClass("visible");
	  	} else {
		 	$(".mapbox-loading-spinner").removeClass("visible");
			$(".mapbox-loading-spinner").addClass("invisible");
	  	}
	});
	$( "#report-link" ).click(function( event ) {
		$().goToIndicatorReport(); 
	});
	$( "button.layer-button:not(.custom-layer-args)" ).click(function( event ) {
		var cardID = $(this).attr("data-card-id");
		var layerID = $(this).attr("data-layer-id");
		var currentlyActiveLayer = $( "button[data-card-id='"+cardID+"'].active" );
		if (currentlyActiveLayer.attr("data-layer-id") == layerID){
			if (activeLayers.hasOwnProperty(cardID)){ //just checking if the layer is already in the map. If it is we skip the function. 
				console.log("Layer Already Active");
				return; 
			} else {
				$().addMapLayer(cardID, '', layerID); 
				return;
			}
		} else {
			//I handled the default layer display poorly, so to make it easy a nice fast and elegant fix is to remove both kinds of potential layers.
			$().removeButtonMapLayer(layerID);
			$().removeMapLayer(cardID);
		}
		$( "button[data-card-id='"+cardID+"']" ).removeClass("active"); //deselects other buttons
		$(this).addClass("active");
		$().addMapLayer(cardID, '', layerID); 
	});
	$( "button.layer-button.custom-layer-args" ).click(function( event ) {
		var cardID = $(this).attr("data-card-id");
		var layerID = $(this).attr("data-layer-id");
		var currentlyActiveLayer = $( "button[data-card-id='"+cardID+"'].active" );
		if (currentlyActiveLayer.attr("data-layer-id") == layerID){
			return;
		} else {
			//I handled the default layer display poorly, so to make it easy a nice fast and elegant fix is to remove both kinds of potential layers.
			$().removeButtonMapLayer(layerID);
			$().removeMapLayer(cardID);
		}
		$( "button[data-card-id='"+cardID+"']" ).removeClass("active"); //deselects other buttons
		$(this).addClass("active");
	});
	
	$('.wrapper-data-card').on('shown.bs.collapse', function () {
				
		//Generic function for every card that opens... It will add a layer unless the layer needs custom arguments. If arguments are needed they must be managed through JS in the card opening event.
		
		var cardID = $(this).attr("id").replace("collapse", "");
		var cardTheme = $(this).closest( ".wrapper-card" ).prev().text().trim();
		var cardLayers = drupalSettings.dataCardsCountry[cardTheme].data[cardID].layers;
		if (typeof cardLayers !== 'undefined') {
			//now we know we have a layer array, but it might just be a legend, so check the first layer has an id OR if it needs a custom argument 
			if (cardLayers.length > 1){
				$(this).find( "button.active" ).trigger("click"); //this will ensure the args for the layer are calculated 
			} else {
				if (typeof cardLayers[0].id !== 'undefined' && cardLayers[0].customArgs !== true) {
					$().addMapLayer(cardID); 
				}
			}
		} 
		 
		
	});
	$('.wrapper-data-card').on('hide.bs.collapse', function () {
			
		//Like above, here's the dynamic layer removal function.
		var cardID = $(this).attr("id").replace("collapse", "");
		var cardTheme = $(this).closest( ".wrapper-card" ).prev().text().trim();
		var cardLayers = drupalSettings.dataCardsCountry[cardTheme].data[cardID].layers;
		if (typeof cardLayers !== 'undefined') {
			//now we know we have a layer array, but it might just be a legend, so check the first layer has an id
			if (typeof cardLayers[0].id !== 'undefined') {
				$().removeMapLayer(cardID);
			}
		}
	});
	
})(jQuery, Drupal);
